# My project's README

This is the readme of the project.

Something that can help:

1. 'git help command' will print an explanation (not always useful)

2. git is very powerful, but flexibility has a price: you can destroy everything 
   if you REALLY want to. However it is difficult to do that by mistake (just avoid --force flag and you will be OK, and there is always the safey net of "git reflog").

3. git + bitbucket (or github): it is impossibile to destroy the history in the remote repository (if the admin enables this feature, of course).

4. with git you can feel free to do what you want with every code: explore,  modify, destroy in all the ways you can think of. 

    * git provides an ISOLATED ENVIRONMENT, you can do what you want (except rm -rf .git)

    * you can always go back to every point in the past




#Lesson 1: work in isolation

1. initialize a local repository: __git init__
    * create a directory and inside do _git init_

2. how to add a file: __git add__ (you will use it also to resolve conflicts!)

    * create a file with some text over few lines, and do _git add filename_

3. doing commits: __git commit -m__ "informative message"

4. find the differences between you version and the saved one: __git diff__ (first usage)

    * do a modification to a file and run _git diff_

    * _git diff_ filename tells you the differences between the committed file and the actual one (this difference is called unstaged modifications)

5. understand where you are: __git status__  and __git log__

    * create another file, type _git status_ to see the files you have modified

    * do 2 commits and see all the history with _git log_

6. go back to old commits, delete local modifications: multiple uses of __git checkout__
   
    * pick an old version, and set the repository in that state with _git checkout HASH_

    * you are now in a special status, _git log_ tells you the past history of a commit: _git checkout master_ will bring you back to the latest commit



#Lesson 2: work as a team

1. clone a repository from bitbucket: __git clone__

2. what is a remote repository, the local image of the remote, and the local
   repository

    * take 2 minutes to explore the history with _git log_

4. commits from and to the server: __git push__ and __git pull__

    * do some modifications, check them with _git diff_, and commit

    * everyone has its own version, try to put it on the server with _git push_
    
    * issues? git stops and ask for human help, by __always__ preserving all your modifications, and tells you what to do

5. solve conflicts with _git add_

6. create branches to work in isolation, but at the same time to get all the updates. Concept of branches

    * create a branch with _git checkout -b branchname_

3. where am I? __git branch -a__ to list all the branches



#Lesson 3

1. the staging area: git stash

2. merge branches

3. undo past commits: git reverse (the good), git reset (the bad), git reset --hard && git push --force (the ugly and very bad).

4. harcore git: git reflog (you are in danger if you are using this)


You will spend 70% of your git time using Lesson 1, 20% Lesson 2 and 10% on lesson 3 (or an entire week if you use the wrong command).



